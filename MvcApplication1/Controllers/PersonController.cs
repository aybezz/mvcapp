﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class PersonController : Controller
    {
        private PersonEntities _entities = new PersonEntities();

        public JsonResult GetPersons()
        {
            Person person = _entities.Persons.First(p => p.Id == 1);
            return Json(person, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Person/

        public ActionResult Index(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
                ViewData["search"] = search;
                return View(_entities.Persons.Where(u => u.FirstName.ToLower().Contains(search.ToLower())));
            }
            else
            {
                return View(_entities.Persons.ToList());
            }
        }

        //
        // GET: /Person/Details/5

        public ActionResult Details(int id)
        {
            var p = new Person
            {
                FirstName = "Ayoub",
                LastName = "Ezzouine"
            };
            return View(p);
        }

        //
        // GET: /Person/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Person/Create

        [HttpPost]
        public ActionResult Create(Person person)
        {
            try
            {
                _entities.Persons.AddObject(person);
                _entities.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Person/Edit/5
 
        public ActionResult Edit(int id)
        {
            Person person = _entities.Persons.SingleOrDefault(p => p.Id == id);
            return View(person);
        }

        //
        // POST: /Person/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collectionn)
        {
            try
            {
                Person person = _entities.Persons.SingleOrDefault(p => p.Id == id);
                UpdateModel(person);

                _entities.SaveChanges();

                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Person/Delete/5
 
        public ActionResult Delete(int id)
        {
            Person person = _entities.Persons.SingleOrDefault(p => p.Id == id);
            return View(person);
        }

        //
        // POST: /Person/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Person person = _entities.Persons.SingleOrDefault(p => p.Id == id);
                _entities.Persons.DeleteObject(person);
                _entities.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // POST: /Person/Delete/5

        [HttpPost]
        public JsonResult DeleteAjax(int id, FormCollection collection)
        {
            try
            {
                Person person = _entities.Persons.SingleOrDefault(p => p.Id == id);
                _entities.Persons.DeleteObject(person);
                _entities.SaveChanges();

                return Json("Record deleted successfully!");
            }
            catch
            {
                return Json("An error has been occured!");
            }
        }
    }
}
