﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcApplication1.Models.Person>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Index</h2>
    <% using(Html.BeginForm("Index", "Person", FormMethod.Get)) { %>
        Enter a name: <%= Html.TextBox("search", ViewData["search"] )%>
        <input type='submit' value='Search' />
    <% } %>
        <%: Html.ActionLink("Clear", "Index") %>
    <table>
        <tr>
            <th></th>
            <th>
                FirstName
            </th>
            <th>
                LastName
            </th>
        </tr>
  <% if (Model.Count() == 0)
     { %>
    <tr>
        <td colspan="4">
            No records match search criteria
        </td>
    </tr>
<% } %>
    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Edit", "Edit", new {  id = item.Id }) %> |
                <%: Html.ActionLink("Details", "Details", new { id = item.Id })%> |
                <%: Html.ActionLink("Delete(Traditional)", "Delete", new { id = item.Id })%> |
                <%: Html.ActionLink("Delete(AJAX)", "DeleteAjax", new { id = item.Id }, new { @class="delete", @id = item.Id })%>
            </td>
            <td>
                <%: item.FirstName %>
            </td>
            <td>
                <%: item.LastName %>
            </td>
        </tr>
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

