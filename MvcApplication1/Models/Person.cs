﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication1.Models
{
    [MetadataType(typeof(Person_Validation))]
    public partial class Person
    {
        public int Age { get; set; }
    }

    public partial class Person_Validation
    {
        [Range(0, Int32.MaxValue, ErrorMessage = "Invalid Number")]
        public int Age { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(50, ErrorMessage = "FirstName may not be longer than 50 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(50, ErrorMessage = "LastName may not be longer than 50 characters")]
        public string LastName { get; set; }
    }
}